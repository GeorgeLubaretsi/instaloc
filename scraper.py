#!/usr/bin/env python
import argparse
import json
import operator
import time
from collections import defaultdict
from urllib.parse import urlencode
from urllib.request import Request, urlopen


class API(object):
    PROFILE_URL = 'https://www.instagram.com/{username}/?__a=1'
    MEDIA_URL = 'https://www.instagram.com/p/{shortcode}/?__a=1'
    GRAPH_URL = 'https://www.instagram.com/graphql/query/'
    FEED_QUERY_ID = '17880160963012870'
    DEFAULT_ITEMS_PER_REQUEST = 12
    SLEEP_BETWEEN_REQUESTS = 0

    def build_request(self, url, params=None):
        """
        Base request builder for Instagram WebAPIs
        :param url: 
        :param params: 
        :return: 
        """

        # Emulate headers to avoid bot-detection
        headers = {
            'Host': 'www.instagram.com',
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) '
                          'Chrome/55.0.2883.87 Safari/537.36',
            'Accept': '*/*',
            'Accept-Language': 'en-US',
            'X-Requested-With': 'XMLHttpRequest',
            'Referer': 'https://www.instagram.com/',
        }

        # Remove empty params. Requests module would do this automagically, but
        # we're sticking to standard library
        if params:
            params = {k: v for k, v in params.items() if v is not None}
            url += '?' + urlencode(params)

        req = Request(url, headers=headers)
        response = urlopen(req)
        response = json.loads(response.read().decode('utf-8'))
        time.sleep(self.SLEEP_BETWEEN_REQUESTS)
        return response

    def graph_request(self, query_id, user_id, first=None, after=None):
        """
        /graphql/query/ request builder
        :param query_id: str id of the graph request type. Ex. 17880160963012870 for the media feed
        :param user_id: int user id
        :param first: int number of items per page
        :param after: str cursor id
        :return: urllib.Response
        """
        query = {
            'query_id': query_id,
            'id': user_id,
            'first': first if first else self.DEFAULT_ITEMS_PER_REQUEST,
            'after': after if after else None
        }
        return self.build_request(url=self.GRAPH_URL, params=query)

    def get_user_id(self, username):
        """
        Get user_id from the username
        :param username: str username
        :return: 
        """
        url = self.PROFILE_URL.format(username=username)
        response = self.build_request(url)
        return response['user']['id']


class Feed(object):
    """
    Feed of the user. I.e. images and videos
    """

    def __init__(self, client, user):
        self.client = client
        self.user = user

    def _build_media_request(self, end_cursor=None):
        response = self.client.graph_request(
            query_id=self.client.FEED_QUERY_ID,
            user_id=self.user.id,
            after=end_cursor if end_cursor else None,
        )
        return response

    def all_media(self, max_pages=2):
        """
        Generator for media feed of the user with pagination
        :param max_pages: int number of pages to scrape. Default: 2
        :return: 
        """
        end_cursor = None
        page_count = 0
        while True:
            response = self._build_media_request(end_cursor)
            page = response['data']['user']['edge_owner_to_timeline_media']
            page_items = [item['node'] for item in page['edges']]
            page_count += 1
            yield page_items

            if max_pages and page_count == max_pages:
                break

            # Grab the cursor for the next page
            if page['page_info']['has_next_page']:
                end_cursor = page['page_info']['end_cursor']
            else:
                break

    def media_with_location(self, max_pages=2):
        """
        All media items with location information
        :param max_pages: int number of pages to scrape. Default: 2
        :return: 
        """
        page_num = 0
        print("Scraping {0} pages of instagram feed".format(max_pages))
        for page in self.all_media(max_pages=max_pages):
            page_num += 1
            print('Page {0}'.format(page_num))
            for item in page:
                print('Scraping {0} media object'.format(item['shortcode']))
                extended_media_url = self.client.MEDIA_URL.format(shortcode=item['shortcode'])
                extended_media = self.client.build_request(extended_media_url)
                extended_media = extended_media['graphql']['shortcode_media']
                if extended_media['location']:
                    yield extended_media


class User(object):
    def __init__(self, client, username):
        self.client = client
        self.username = username
        self.id = self.client.get_user_id(username)

    @property
    def feed(self):
        return Feed(self.client, self)


def process(username):
    client = API()
    user = User(client, username)
    items_with_location = []

    for item in user.feed.media_with_location(max_pages=2):
        items_with_location.append(item)

    # defaultdict that keeps count of state mentions in
    # the feed
    states_count = defaultdict(int)

    # Instead of hitting instagram's /location/ endpoint,
    # Grab the data from Google's geocoding service
    for item in items_with_location:
        params = {'address': item['location']['name']}
        geocoding_url = 'http://maps.google.com/maps/api/geocode/json'
        geocoding_url += '?' + urlencode(params)

        response = urlopen(geocoding_url)
        response = json.loads(response.read().decode('utf-8'))

        for comp in response['results'][0]['address_components']:
            if 'administrative_area_level_1' in comp['types']:
                states_count[comp['long_name']] += 1

    # Get state with the most mentions
    if states_count:
        state = max(states_count.items(), key=operator.itemgetter(1))[0]
        print('\n\nScraping complete. State: {0}'.format(state))
    else:
        print('\n\nUnable to identify location.')


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('username', type=str)
    args = parser.parse_args()
    process(args.username)
